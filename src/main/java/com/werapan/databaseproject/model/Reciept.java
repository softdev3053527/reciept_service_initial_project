/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author slmfr
 */
public class Reciept {
    
    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetail = new ArrayList<RecieptDetail>();

    public Reciept(int id, Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }
   

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0l;
        this.cash = 0;
        this.totalQty =0;
        this.userId = 0;
        this.customerId = 0;


    }

    public int getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public float getTotal() {
        return total;
    }

    public float getCash() {
        return cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public User getUser() {
        return user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId =customer.getId();
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + '}';
    }

    public ArrayList<RecieptDetail> getRecieptDetail() {
        return recieptDetail;
    }

    public void setRecieptDetail(ArrayList<RecieptDetail> recieptDetail) {
        this.recieptDetail = recieptDetail;
    }
    
    public void addRecieptDetail(RecieptDetail rd){
            this.recieptDetail.add(rd);
            caculateTotal();
    }
    
     public void addRecieptDetail(Product product,int qty){
            RecieptDetail rd = new RecieptDetail(product.getId(),product.getName(),product.getPrice(),qty,product.getPrice()*qty,-1);
            this.recieptDetail.add(rd);
            caculateTotal();
    }
         public void delRecieptDetail(RecieptDetail rd){
            this.recieptDetail.remove(rd);
            caculateTotal();
    }
     
    
    public void caculateTotal(){
        int totalQty = 0;
        float total = 0.0f;
        for(RecieptDetail rd : recieptDetail){
            totalQty =totalQty + rd.getQty();
            total =total + rd.getTotalPrice();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

   


        public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("rec_id"));
            reciept.setCreatedDate(rs.getTimestamp("rec_date"));
            reciept.setTotal(rs.getFloat("rec_total"));
            reciept.setCash(rs.getFloat("rec_cash"));
            reciept.setTotalQty(rs.getInt("rec_total_qty"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            reciept.setUserId(rs.getInt("user_id"));
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer =  customerDao.get(reciept.getCustomerId());
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecieptDetailDao;
import com.werapan.databaseproject.dao.RecieptDetailDao;

import com.werapan.databaseproject.model.RecieptDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDetailService {
    public  List<RecieptDetail> getById(int id){
        RecieptDetailDao recieptDetailDao =new RecieptDetailDao();
        return  recieptDetailDao.getAll("rec_id = "+id, "reciept_detail_id");
    }

    public List<RecieptDetail> getRecieptDetails(){
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getAll(" rec_id asc");
    }

    public RecieptDetail addNew(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.save(editedRecieptDetail);
    }

    public RecieptDetail update(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.update(editedRecieptDetail);
    }

    public int delete(RecieptDetail editedRecieptDetail) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.delete(editedRecieptDetail);
    }
}
